(ns guardian-alerts.core
  (:require [cheshire.core :as json])
  (:gen-class))

(def service "https://content.guardianapis.com/search")

(defn get-config []
  (json/parse-string (slurp (.getPath (clojure.java.io/resource "config.json"))) true))
 
(defn get-api-key []
  (get (get-config) :api-key))

(defn today-date []
  (.format (java.text.SimpleDateFormat. "yyyy-MM-dd") (java.util.Date.)))

(defn build-url
  ([query]
   (let [api-key (get-api-key)]
     (build-url query api-key)))

  ([query akey]
   (let [t (today-date)]
    (str service "?from-date=" t "&api-key=" akey "&q=" query))))

(defn search [query]
  (let [jstr (slurp (build-url query))]
    (when-let [resp (json/parse-string jstr true)]
      (get resp :response))))


(defn print-articles
  [articles]
  (doseq [a articles]
    (println (:webTitle a))
    (println "  Section: " (:sectionName a))
    (println "  Type: " (:type a))
    (println "----")))
  

;; for resources see
;; https://stackoverflow.com/questions/8009829/resources-in-clojure-applications
;; (.getPath (clojure.java.io/resource "config"))
(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  ;[[qry from-date] args]
  ;(println "query: " qry)
  ;(println "from-date: " from-date)
  (when-first [query args]
    (try
      (let [encoded-query (clojure.string/replace query " " "%20")
            search-response (search encoded-query)]
        (println "Found " (str (get search-response :total) " article(s):"))
        (print-articles (get search-response :results)))
      (catch Exception e (str "caught exception: " (.getMessage e))))))

     
    
    
      
    

    
